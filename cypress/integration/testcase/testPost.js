context('Test Gowabi', () => {

    describe('Promotion suite', () => {
  
      it('Direct discount detail page', () => {
        cy.visit('https://www.gowabi.com')
        cy.get('[src="https://www.gowabi.com/system/mobile_banners/images/000/000/051/category_banner_top_pic/Promotions-App-Banner-2021_03.jpg?1614964060"]').scrollIntoView().click()
        cy.get('[data-organization-service-name="Nano Mink 3D Volume Eyelash Extension (Unlimited Strands)"]').click()
      })

      it('Searching by keyword', () => {
        cy.visit('https://www.gowabi.com')
        cy.get('#autocomplete_search_field').type('nail polish')
        cy.get('button[type="submit"]').click()
      })

    })
  })
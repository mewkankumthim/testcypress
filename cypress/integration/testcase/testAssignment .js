context('Test Google', () => {

    describe('Test Search on Google', () => {
  
      it('Search text on Google ', () => {
        cy.visit('http://automationpractice.com/')
        cy.get('#block_top_menu>ul>li>a[title="T-shirts"]').click()
        cy.get('#pagination').scrollIntoView()
        cy.get('a[title="Add to cart"]').click()
        cy.get('span[class="cross"]').click()
        cy.get('#header').scrollIntoView()
        cy.get('a[title="View my shopping cart"]').click()
        cy.get('#center_column').scrollIntoView()
        cy.get('#cart_quantity_up_1_1_0_0').click().click().click()
      })
  
    })
  
  })